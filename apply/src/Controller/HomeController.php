<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

#[Route('/order/{maVar}' , name:'test.order.route')]
public function testOrderRoute($maVar){
    return new Response($maVar);
}


    #[Route('/home', name: 'home')]
    public function index(): Response
    {
        return $this->render('home/index.html.twig', [
            'name' => 'bastard',
            'firstname' => 'greg'
        ]);
    }
    #[Route('/sayhello/{name}', name: 'say.hello')]
    public function sayhello(Request $request, $name): Response
    {
        
        return $this->render('home/hello.html.twig', ['nom' => $name]);
    }
    #[Route(
        'multi/{entier1<\d+>}/{entier2<\d+>}',
        name: 'multiplication',
        
    )]
    public function multiplication($entier1, $entier2)
    {
        $resultat = $entier1 * $entier2;
        return new Response("<h1>$resultat</h1>");
    }
}
